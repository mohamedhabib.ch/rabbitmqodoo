
from odoo import  fields ,models

import pika




class RabbitmqOdoo(models.Model):
    _name='rabbitmq.odoo'
    
    
    name=fields.Char(string='Message')



    def action_publish(self):
        credentials = pika.PlainCredentials('test', 'test')
        parameters = pika.ConnectionParameters('localhost',15672,'/',credentials)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='logs')
        channel.exchange_declare(exchange='logs', exchange_type='direct')
        message = self.name
        channel.basic_publish(exchange='logs', routing_key='logs', body=message)
        
        
        
        return True

