{
    'name': 'Rabbit MQ TEST MODULE ',
    'version': '15.0.1.0.0',
    'summary': '',
    'category': '',
    'author': 'Mohamed Habib CHALLOUF',
    'maintainer': '',
    'website': '',
    'license': 'LGPL-3',
    'contributors': [
    ],
    'depends': [
        'base',
        'web',
        
    ],
    'data': [
        "views/rabbitmq_views.xml",
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}